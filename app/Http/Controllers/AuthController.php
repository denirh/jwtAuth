<?php

namespace App\Http\Controllers;

use Hash;
use JWTAuth;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct(User $user)
    {
    	$this->user = $user;
    }

    public function login(Request $request)
    {
    	$getData = $request->only(['email', 'password']);

    	if(!$token = JWTAuth::attempt($getData)){
    		return response()->json(['error' => 'Invalid Credentials!'], 401);
    	}
    	return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
    	$storeData = $request->only(['name', 'email', 'password']);

    	$storeData = [
    		'name' => $storeData['name'],
    		'email' => $storeData['email'],
    		'password' => Hash::make($storeData['password']),
    	];

    	try
    	{
    		$user = $this->user->create($storeData);
    	}catch(Exception $e){
    		return response()->json(['error' => 'User already exist !'], 409);
    	}

    	$token = JWTAuth::fromUser($user);
    	return response()->json(compact('token'));
    }

    public function show()
    {
    	return response()->json(JWTAuth::toUser(JWTAuth::getToken()));
    }

    public function logout()
    {
    	JWTAuth::invalidate(JWTAuth::getToken());
    	return response()->json(['Message' => 'Token already invalidated'], 200);
    }
}
